# Notes on building mrs_denoising_tools

## Prep
1. Commit code changes
2. Increment version number in setup.cfg
3. Commit incremented version.
4. Clean working directory.  
    `git clean -fdxn -e *.code-workspace`  
    `git clean -fdx -e *.code-workspace`

## Build and upload to Pypi
In enviroment with:
`conda install -c conda-forge python-build twine`

1. `python -m build`
2. `twine upload dist/*`

## Conda-forge
Wait for automated tick bot to create a new PR.  
Feedstock [Github page](https://github.com/conda-forge/mrs_denoising_tools-feedstock).